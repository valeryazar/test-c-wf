﻿using System;
using System.Windows.Forms;

namespace Testing
{
    public partial class Form1 : Form
    {
        int Score_Of_Questions; // Счет вопросов
        int The_Number_Of_Correct_Answers; // Количество правильных ответов
        int The_Number_Of_Incorrect_Answers; // Количество не правильных ответов
        String[] Wrong_Answers;
        int The_Correct_Answer_Number; // Номер правильного ответа
        int Answer_Number; // Номер ответа
        System.IO.StreamReader Reader;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            button1.Text = "Следующий вопрос";
            button2.Text = "Выход";
            radioButton1.CheckedChanged += new EventHandler(Next_Question);
            radioButton2.CheckedChanged += new EventHandler(Next_Question);
            radioButton3.CheckedChanged += new EventHandler(Next_Question);
            radioButton4.CheckedChanged += new EventHandler(Next_Question);
            Start_Testing();
        }

        void Start_Testing()
        {
            var Encoding = System.Text.Encoding.GetEncoding(1251);
            try
            {
                Reader = new System.IO.StreamReader(System.IO.Directory.GetCurrentDirectory() + @"\Українська мова.txt", Encoding);
                this.Text = Reader.ReadLine();
                Score_Of_Questions = 0;
                The_Number_Of_Correct_Answers = 0;
                The_Number_Of_Incorrect_Answers = 0;
                Wrong_Answers = new String[100];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); // Отчет о всех ошибках
            }
            Read_Next_Question();
        }

        void Read_Next_Question()
        {
            label1.Text = Reader.ReadLine();
            radioButton1.Text = Reader.ReadLine();
            radioButton2.Text = Reader.ReadLine();
            radioButton3.Text = Reader.ReadLine();
            radioButton4.Text = Reader.ReadLine();
            The_Correct_Answer_Number = int.Parse(Reader.ReadLine());
            radioButton1.Checked = false;
            radioButton2.Checked = false;
            radioButton3.Checked = false;
            button1.Enabled = false;
            Score_Of_Questions += 1;
            if (Reader.EndOfStream == true)
            {
                button1.Text = "Завершить";
            }
        }
        void Next_Question(Object sender, EventArgs e)
        {
            button1.Enabled = true; button1.Focus();
            RadioButton Switch = (RadioButton)sender;
            var tmp = Switch.Name;
            Answer_Number = int.Parse(tmp.Substring(11));
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (Answer_Number == The_Correct_Answer_Number)
            {
                The_Number_Of_Correct_Answers += 1;
            }
            if (Answer_Number == The_Correct_Answer_Number)
            {
                The_Number_Of_Incorrect_Answers += 1;
                Wrong_Answers[The_Number_Of_Incorrect_Answers] = label1.Text;
            }
            if (button1.Text == "Начать тестирование сначала")
            {
                button1.Text = "Следующий вопрос";
                radioButton1.Visible = true;
                radioButton2.Visible = true;
                radioButton3.Visible = true;
                radioButton4.Visible = true;
                Start_Testing();
                return;
            }
            if (button1.Text == "Завершить")
            {
                Reader.Close();
                radioButton1.Visible = false;
                radioButton2.Visible = false;
                radioButton3.Visible = false;
                radioButton4.Visible = false;
                label1.Text = String.Format("Тестирование завершено.\n" + "Правильных ответов: {0} из {1}.\n" + "Оценка: {2:F2}.", The_Number_Of_Correct_Answers, Score_Of_Questions, (The_Number_Of_Correct_Answers * 10.0F) / Score_Of_Questions);
                button1.Text = "Начать тестирование сначала";
            }
            if (button1.Text == "Следующий вопрос")
            {
                Read_Next_Question();
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}